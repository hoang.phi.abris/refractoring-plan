# Refractoring Plan

## Environment

We will need at least 2 (optimal is 4) seperate enviroment.
- Staging - Production
- Dev (close to local env - for code integrate) - Test (environments used for manual and/or automated tests) - Stagting (almost identical with production -  used for acceptance testing before pushing new code to production) - Production

**Stratergy**: Branching stratergy - Each branch match each env
create feature udapte branch -> new MR

## Question

- How often are we going to release your apps?
- Do we need to create end-to-end automation?
- Do we need to use orchestration tools?

## Flow
 code quality → unit tests → build → artifact repo -> staging deployment → e2e test(optional)

 Focus on implementing **CI first**. After the CI is proved to be reliable -> CD

 ## More question

- How often is the scenario repeated?
- How long is the CI-CD process?
- What people and resource dependencies are involved in the process? Are they causing delays in CI/CD?

## Priority
- Build only once 
- Reduce branching through small and incremental commits 
- Plan test priority carefully
- Isolate CI/CD pipeline
- Deploy via CI/CD only

## Docker CI

 - the **importance** of splitting out the things that need to be there in order to compile your program from the things that only need to be there when the program runs. Very often, **a lot of the stuff involved in a project is only required to compile or build i**t, and not really **required at runtime**. 
 - Avoid Inclusion of build tools in final images
 - Porudction images should be as small as possible
 - Pre-cache common dependencies in build images

 Example:
 - 2 registry - one for builder - one for actual application 

 **Example builder java**
 ```
 FROM openjdk:8
RUN echo 1
RUN apt-get update && apt-get install  -y curl
RUN curl -o /usr/bin/lein https://raw.githubusercontent.com/technomancy/leiningen/stable/bin/lein
RUN chmod +x /usr/bin/lein
 ```
```
docker build -t registry/name-builder:${git rev-parse HEAD} -f Dockerfile.builder .
```
create a shared dir between where I am and inside docker container -> build artifact be left in my computer - not in clean up docker
```
docker run --rm -v "$PWD:/work" image::${git rev-parse HEAD} bash -C "cd /work; ./grandlew uberjar"
```

 **Example app**

 ```
 FROM java:8-alpine
MAINTAINER abc

ADD target/uberjar/example-webapp.jar /example-webapp/app.jar

EXPOSE 3000

CMD ["java", "-jar", "/example-webapp/app.jar"]
 ```



